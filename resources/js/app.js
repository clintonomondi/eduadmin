import Vue from 'vue'
import VueRouter from 'vue-router'
import VueAxios from 'vue-axios';
import axios from 'axios';
import VueTelInput from 'vue-tel-input'
import Notifications from 'vue-notification'
import JwPagination from 'jw-vue-pagination';

Vue.use(VueRouter);
Vue.use(VueAxios, axios);
Vue.use(VueTelInput);
Vue.use(Notifications);
Vue.component('jw-pagination', JwPagination);

import App from './view/App';
import Login from './pages/login';
import Home from './pages/home';
import CategoryIndex from './category/index';
import CategoryEdit from './category/edit_category';
import Add_category from './category/add_category';
import Subject from './subject/index';
import Classes from './classes/index';
import Teachers from './teacher/index';
import MpesaTrans from './transaction/mpesa';
import Taxes from './tax/taxes';





const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'login',
            component: Login,
            meta: { hideNavigation: true }
        },
        {
            path: '/home',
            name: 'home',
            component: Home
        },
        {
            path: '/category',
            name: 'category',
            component: CategoryIndex
        },
        {
            path: '/add_category',
            name: 'add_category',
            component: Add_category
        },
        {
            path: '/subjects/:id',
            name: 'subjects',
            component: Subject
        },
        {
            path: '/classes/:id',
            name: 'classes',
            component: Classes
        },
        {
            path: '/teachers',
            name: 'teachers',
            component: Teachers
        },
        {
            path: '/category/edit/:id',
            name: 'editcategory',
            component: CategoryEdit
        },
        {
            path: '/transaction/mpesa',
            name: 'mpesa',
            component: MpesaTrans
        },
        {
            path: '/taxes',
            name: 'taxes',
            component: Taxes
        },
    ],
});


const app = new Vue({
    el: '#app',
    components: { App },
    router,

});

