<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keywords" content="MediaCenter, Template, eCommerce">
    <meta name="robots" content="all">
    <title>Edu-Bora</title>

    <!-- plugins:css -->
    <link rel="stylesheet" href="/asset/vendors/ti-icons/css/themify-icons.css">
    <link rel="stylesheet" href="/asset/vendors/css/vendor.bundle.base.css">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="/asset/css/vertical-layout-light/style.css">

    <link rel="stylesheet" href="/asset/vendors/font-awesome/css/font-awesome.min.css" />
    <!-- endinject -->
</head>
<body>
<div id="app">
    <app></app>
</div>
<script src="{{ mix('js/app.js') }}"></script>


<script src="/asset/vendors/js/vendor.bundle.base.js"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<script src="/asset/vendors/jquery-steps/jquery.steps.min.js"></script>
<script src="/asset/vendors/jquery-validation/jquery.validate.min.js"></script>
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="/asset/js/off-canvas.js"></script>
<script src="/asset/js/hoverable-collapse.js"></script>
<script src="/asset/js/template.js"></script>
<script src="/asset/js/settings.js"></script>
<script src="/asset/js/todolist.js"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script src="/asset/js/wizard.js"></script>
<!-- End custom js for this page-->

{{--<script src="/js/wizard.js"></script>--}}
<script src="/js/buttonloader.js"></script>
<script src="/asset/vendors/jquery-toast-plugin/jquery.toast.min.js"></script>
<!-- Custom js for this page-->
<script src="/asset/js/toastDemo.js"></script>
{{--<script src="/asset/js/desktop-notification.js"></script>--}}
<!-- End custom js for this page-->
<script src="/asset/vendors/chart.js/Chart.min.js"></script>


{{--<script src="/asset/js/off-canvas.js"></script>--}}
{{--<script src="/asset/js/hoverable-collapse.js"></script>--}}
{{--<script src="/asset/js/template.js"></script>--}}
{{--<script src="/asset/js/settings.js"></script>--}}
{{--<script src="/asset/js/todolist.js"></script>--}}
<script src="/asset/js/chart.js"></script>
<script src="/asset/js/desktop-notification.js"></script>
<script src="/loader/center-loader.js"></script>
</body>
</html>
